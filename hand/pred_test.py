import tensorflow as tf
import numpy as np
from utils import cpm_utils, tracking_module, utils
import cv2
import time
import math
import importlib
import os
import struct
import io
from collections import deque
import threading
import socket
import socketserver
import itertools
from ya_config import FLAGS


# socket 정보를 담을 dict 생성
clients = {}

# 이미지를 담을 queue 생성
img_que = deque()
# hand coord를 담을 queue 생성
coord_que = deque()
# action coord array를 담을 queue 생성
pred_que = deque()

HOST, PORT = 'PC IP', 9008 # server 연결

cpm_model = importlib.import_module('models.nets.' + FLAGS.network_def)

joint_detections = np.zeros(shape=(21, 2)) # 카메라 tracking 용도
local_detections = np.zeros(shape=(42, 1)) # hand coord 

lock = threading.RLock()
stop_thread = threading.Event()

def main():
    global joint_detections
    os.environ['CUDA_VISIBLE_DEVICES'] = "0"

    tracker = tracking_module.SelfTracker([FLAGS.webcam_height, FLAGS.webcam_width], FLAGS.input_size)

    model = cpm_model.CPM_Model(input_size=FLAGS.input_size,
                                heatmap_size=FLAGS.heatmap_size,
                                stages=FLAGS.cpm_stages,
                                joints=FLAGS.num_of_joints,
                                img_type=FLAGS.color_channel,
                                is_training=False)

    output_node = tf.get_default_graph().get_tensor_by_name(name=FLAGS.output_node_names)

    sess_config=tf.ConfigProto(device_count = {'GPU': 1})
    sess_config.gpu_options.per_process_gpu_memory_fraction = 0.6
    sess_config.gpu_options.allow_growth = True

    with tf.Session(config=sess_config) as sess:
        with tf.device('/gpu:0'):
            model_path_suffix = os.path.join(FLAGS.network_def,
                                            'input_{}_output_{}'.format(FLAGS.input_size, FLAGS.heatmap_size),
                                            'joints_{}'.format(FLAGS.num_of_joints),
                                            'stages_{}'.format(FLAGS.cpm_stages),
                                            'init_{}_rate_{}_step_{}'.format(FLAGS.init_lr, FLAGS.lr_decay_rate,
                                                                            FLAGS.lr_decay_step)
                                            )
            model_save_dir = os.path.join('models',
                                        'weights',
                                        model_path_suffix)
            print('Load model from [{}]'.format(os.path.join(model_save_dir, './models/weights/cpm_hand.pkl')))

            model.load_weights_from_file('./models/weights/cpm_hand.pkl', sess, False)

            # Create kalman filters
            if FLAGS.use_kalman:
                kalman_filter_array = [cv2.KalmanFilter(4, 2) for _ in range(FLAGS.num_of_joints)]
                for _, joint_kalman_filter in enumerate(kalman_filter_array):
                    joint_kalman_filter.transitionMatrix = np.array(
                        [[1, 0, 1, 0], [0, 1, 0, 1], [0, 0, 1, 0], [0, 0, 0, 1]],
                        np.float32)
                    joint_kalman_filter.measurementMatrix = np.array([[1, 0, 0, 0], [0, 1, 0, 0]], np.float32)
                    joint_kalman_filter.processNoiseCov = np.array([[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]],
                                                                np.float32) * FLAGS.kalman_noise
            else:
                kalman_filter_array = None

            # picamera와 socket 연결
            socket_cam = socket.socket()
            socket_cam.bind(('0.0.0.0', FLAGS.cam_port))
            socket_cam.listen(0)
            print("waiting camera connection")
            conn_cam = socket_cam.accept()[0].makefile('rb')

            # socketserver 실행
            server = SocksServer((HOST, PORT))
            server_thread = threading.Thread(target=server.serve_forever)
            server_thread.daemon=True
            server_thread.start()
            
            # camera로부터 image를 실시간으로 계속 저장하는 thread
            p1 = threading.Thread(target=save_img ,args=(clients,conn_cam, socket_cam))
            p1.daemon = True 
            p1.start()

            # pred_que에서 실시간으로 action을 예측하는 thread
            p2 = threading.Thread(target=new_sess, args=(pred_que,clients))
            p2.daemon = True 
            p2.start()
            index=1
            while True:
                if len(img_que)>0:
                    full_img = img_que.pop()
                    test_img = tracker.tracking_by_joints(full_img, joint_detections=joint_detections)
                    crop_full_scale = tracker.input_crop_ratio

                    test_img_input = normalize_and_centralize_img(test_img)

                    # Inference
                    stage_heatmap_np = sess.run([output_node],
                                                feed_dict={model.input_images: test_img_input})

                    visualize_result(full_img, stage_heatmap_np, kalman_filter_array, tracker, crop_full_scale)
                    #cv2.imshow('local_img', local_img.astype(np.uint8))
                    cv2.imshow('global_img', full_img.astype(np.uint8))

                    #f=open("./test{}.txt".format(index),"w")
                
                    # 한 이미지(frame)에서 추출한 coord값을 coord_que에 넣는다.
                    coord_que.append(local_detections)
                    #if 'switch1' in clients.keys():
                    #    clients['switch1'].send(str(local_detections).encode())

                    # 45 프레임이 모이면 동작예측을 위한 pred_que로 전달
                    if len(coord_que) >= 45:
                        pred_que.appendleft(list(itertools.islice(coord_que, 45)))
                        coord_que.popleft() # 한 프레임씩 sliding window를 위한 pop
                    
                    index+=1
                    if cv2.waitKey(1) == ord('q'): 
                        cv2.destroyAllWindows()
                        stop_thread.set()
                        break
            #p1.join()
            #p2.join()
    
# action을 예측하는 함수
def new_sess(ck_class, clients):
    
    classes = list(range(8))# 예측할 클래스의 개수

    sess_config3 = tf.ConfigProto(log_device_placement=True)
    sess_config3.gpu_options.per_process_gpu_memory_fraction = 0.6
    sess_config3 = tf.ConfigProto(device_count = {'GPU': 1})
    sess_config3.gpu_options.allow_growth = True

    prediction = deque() # 연속 동작을 모을 queue
    hand_pose = None
    before_hand_pose = 10 # 연속된 동작을 방지하기 위해 이전 pose 기록
    frame_cnt = 0
    target_order={}
    
    f = open("./test.txt","w")

    # CNN 모델을 불러와서 예측한다.
    with tf.Session(graph=tf.Graph(), config=sess_config3) as sess:
        new_saver = tf.train.import_meta_graph('../train/model/cnn+RandomNone.ckpt-195300.meta')
        new_saver.restore(sess, '../train/model/cnn+RandomNone.ckpt-195300')
        #new_saver = tf.train.import_meta_graph('../../train2/model/cnn.ckpt-28800.meta')
        #new_saver.restore(sess, '../../train2/model/cnn.ckpt-28800')
        
        graph = tf.get_default_graph() 
        # 예측에 필요한 pred와 X 변수 
        pred = graph.get_tensor_by_name("pred:0")
        X = graph.get_tensor_by_name("X:0")
        keep_prob = graph.get_tensor_by_name("Placeholder:0")
        var_check = [6,7,8,9,14,15,16,17,22,23,24,25,30,31,32,33,38,39,40,41] # 손가락 끝부분 위치
        while not stop_thread.is_set():
            print("len ", len(pred_que), end="\r")
            start = time.time()
            
            if len(pred_que) >0:
                lock.acquire()
                pred_class = pred_que.pop()
                # 손의 움직임을 분산으로 먼저 체크, 움직이지 않으면 분산이 작다는 원리를 활용
                if np.var(pred_class,axis=0)[var_check].mean() > 20: 
                    _pred = sess.run(pred, feed_dict={X: [np.transpose(pred_class,[1,0,2])], keep_prob:1})
                    #_pred = sess.run(pred, feed_dict={X: [np.transpose(pred_class,[1,0,2])]})
                else:
                    lock.release()
                    continue
                
                # threshold를 0.8로 설정
                # threshold가 한 번 0.8이 되면 이후 연속 9동작을 threshold 판단없이 모은다.
                
                if np.argmax(_pred) != 7 and np.max(_pred) > 0.95 or frame_cnt>0:
                    prediction.appendleft(np.argmax(_pred))
                    frame_cnt+=1
                # 11동작이 되면 max값이 제일 많이 나온 동작으로 판단한다.  
                if len(prediction) == 9:
                    res = {i:prediction.count(i) for i in classes} # class 별 동작 분류
                    hand_pose = np.argmax(list(res.values()))
                    prediction.clear() # prediction que 초기화
                    frame_cnt=0
                    if before_hand_pose!=hand_pose:
                        if 'switch1' in clients.keys():
                            clients['switch1'].send(str(hand_pose).encode())
                    before_hand_pose=hand_pose
                        
                lock.release()

# 이미지를 저장하는 함수
def save_img(clients, conn_cam, socket_cam):
    print("Start saving image")
    while not stop_thread.is_set():
        image_len = struct.unpack('<L', conn_cam.read(struct.calcsize('<L')))[0]
        if not image_len:
            print("image saving end")
            conn_cam.close()
            socket_cam.close()
            break
        image_stream = io.BytesIO()
        image_stream.write(conn_cam.read(image_len))
        image_stream.seek(0)

        # Stream 데이터를 opencv로 읽는다.
        file_bytes = np.asarray(bytearray(image_stream.read()), dtype=np.uint8)
        load_img = cv2.imdecode(file_bytes, cv2.IMREAD_COLOR)

        # image queue에 삽입한다.
        img_que.appendleft(load_img)

def normalize_and_centralize_img(img):
    if FLAGS.normalize_img:
        test_img_input = img / 256.0 - 0.5
        test_img_input = np.expand_dims(test_img_input, axis=0)
    else:
        test_img_input = img - 128.0
        test_img_input = np.expand_dims(test_img_input, axis=0)
    return test_img_input


def visualize_result(test_img, stage_heatmap_np, kalman_filter_array, tracker, crop_full_scale):
 
    last_heatmap = stage_heatmap_np[len(stage_heatmap_np) - 1][0, :, :, 0:FLAGS.num_of_joints].reshape((FLAGS.heatmap_size, FLAGS.heatmap_size, FLAGS.num_of_joints))
    last_heatmap = cv2.resize(last_heatmap, (FLAGS.input_size, FLAGS.input_size))

    correct_and_draw_hand(test_img, last_heatmap, kalman_filter_array, tracker, crop_full_scale)

def correct_and_draw_hand(full_img, stage_heatmap_np, kalman_filter_array, tracker, crop_full_scale):
    global joint_detections
    global local_detections
    joint_coord_set = np.zeros((FLAGS.num_of_joints, 2))
    local_joint_coord_set = np.zeros((FLAGS.num_of_joints, 2))

    mean_response_val = 0.0

    # Plot joint colors
    if kalman_filter_array is not None:
        for joint_num in range(FLAGS.num_of_joints):
            tmp_heatmap = stage_heatmap_np[:, :, joint_num]
            joint_coord = np.unravel_index(np.argmax(tmp_heatmap),
                                           (FLAGS.input_size, FLAGS.input_size))
            mean_response_val += tmp_heatmap[joint_coord[0], joint_coord[1]]
            joint_coord = np.array(joint_coord).reshape((2, 1)).astype(np.float32)
            kalman_filter_array[joint_num].correct(joint_coord)
            kalman_pred = kalman_filter_array[joint_num].predict()
            correct_coord = np.array([kalman_pred[0], kalman_pred[1]]).reshape((2))
            local_joint_coord_set[joint_num, :] = correct_coord

            # Resize back
            correct_coord /= crop_full_scale

            # Substract padding border
            correct_coord[0] -= (tracker.pad_boundary[0] / crop_full_scale)
            correct_coord[1] -= (tracker.pad_boundary[2] / crop_full_scale)
            correct_coord[0] += tracker.bbox[0]
            correct_coord[1] += tracker.bbox[2]
            joint_coord_set[joint_num, :] = correct_coord
    
    draw_hand(full_img, joint_coord_set, tracker.loss_track)
    #draw_hand(crop_img, local_joint_coord_set, tracker.loss_track)
    joint_detections = joint_coord_set

    # 손을 catch하면 coord를 입력받고 아니면 0을 넣는다.
    if tracker.loss_track == False:
        local_detections = np.transpose(np.reshape([[round(coord[1],3), round(coord[0],3)] for coord in local_joint_coord_set],[1,-1]))
    else:
        local_detections = np.zeros((42,1))    

    if mean_response_val >= 0.7:
        tracker.loss_track = False
    else:
        tracker.loss_track = True

def draw_hand(full_img, joint_coords, is_loss_track):
    if is_loss_track:

        joint_coords = FLAGS.default_hand

    # Plot joints
    for joint_num in range(FLAGS.num_of_joints):
        color_code_num = (joint_num // 4)
        if joint_num in [0, 4, 8, 12, 16]:
            joint_color = list(map(lambda x: x + 35 * (joint_num % 4), FLAGS.joint_color_code[color_code_num]))
            cv2.circle(full_img, center=(int(joint_coords[joint_num][1]), int(joint_coords[joint_num][0])), radius=3,
                       color=joint_color, thickness=-1)
        else:
            joint_color = list(map(lambda x: x + 35 * (joint_num % 4), FLAGS.joint_color_code[color_code_num]))
            cv2.circle(full_img, center=(int(joint_coords[joint_num][1]), int(joint_coords[joint_num][0])), radius=3,
                       color=joint_color, thickness=-1)

    # Plot limbs
    for limb_num in range(len(FLAGS.limbs)):
        x1 = int(joint_coords[int(FLAGS.limbs[limb_num][0])][0])
        y1 = int(joint_coords[int(FLAGS.limbs[limb_num][0])][1])
        x2 = int(joint_coords[int(FLAGS.limbs[limb_num][1])][0])
        y2 = int(joint_coords[int(FLAGS.limbs[limb_num][1])][1])
        length = ((x1 - x2) ** 2 + (y1 - y2) ** 2) ** 0.5
        if length < 150 and length > 5:
            deg = math.degrees(math.atan2(x1 - x2, y1 - y2))
            polygon = cv2.ellipse2Poly((int((y1 + y2) / 2), int((x1 + x2) / 2)),
                                       (int(length / 2), 3),
                                       int(deg),
                                       0, 360, 1)
            color_code_num = limb_num // 4
            limb_color = list(map(lambda x: x + 35 * (limb_num % 4), FLAGS.joint_color_code[color_code_num]))
            cv2.fillConvexPoly(full_img, polygon, color=limb_color)

# sockerserver handler
class SocksHandler(socketserver.BaseRequestHandler):
    conn_info = ''

    def setup(self):
        self.conn_info = self.request.recv(512).decode().strip()
        clients[self.conn_info] = self.request
        print(clients[self.conn_info])

    def handle(self):
        self.request.send((self.conn_info + " is connected with server").encode())
        msg = self.request.recv(512).decode()

    def finish(self):
        print(self.conn_info+"disconnected")
        del clients[self.conn_info]
        self.request.close()

class SocksServer(socketserver.ThreadingTCPServer):
    
    def __init__(self, listen_addr):
        socketserver.ThreadingTCPServer.__init__(self, listen_addr, SocksHandler)
        self.allow_reuse_address = True
        self.request_queue_size = 5

if __name__ == '__main__':
    main()
