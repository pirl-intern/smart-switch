/*전시?용
알림음 기능만 있는 애플리케이션*/

package com.example.pirl.yurim;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

public class MainActivity extends AppCompatActivity {

    private BackPressCloseHandler backPressCloseHandler;
    private AudioManager mAudioManager;
    private Socket socket;
    BufferedReader in;private BufferedReader bufferedReader1;
    private BufferedWriter bufferedWriter1;
    private PrintWriter sockPrintWriter;
    private Handler mHandler;
    private static String line;
    MediaPlayer mPlayer;

    private EditText edittext1, edittext2;
    private ImageButton imageButton, imageButton2;

    private String ip, port;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mHandler = new Handler();

        imageButton = (ImageButton)findViewById(R.id.imageButton); //음악 정지 버튼
        imageButton.setOnClickListener(buttonListener);

        edittext1 = (EditText)findViewById(R.id.editText1); //ip 입력받는 editText
        edittext2 = (EditText)findViewById(R.id.editText2); //port 입력받는 editText

        imageButton2 = (ImageButton)findViewById(R.id.imageButton2); //소켓 연결 버튼
        imageButton2.setOnClickListener(buttonListener2);

        mAudioManager = (AudioManager)getSystemService(AUDIO_SERVICE); //오디오 객체 생성
        mAudioManager.setStreamVolume(mAudioManager.STREAM_MUSIC, 100, 0); //시스템의 음악 볼륨 최대

        mPlayer = MediaPlayer.create(MainActivity.this, R.raw.sound2); //노래 파일 생성

        backPressCloseHandler = new BackPressCloseHandler(this);
    }

    protected void onStop() {
        super.onStop();
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    View.OnClickListener buttonListener2 = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            ip = edittext1.getText().toString(); //레이아웃의 텍스트뷰에 입력한 값을 ip라는 string 변수에 저장
            port = edittext2.getText().toString(); //마찬가지로 port라는 string 변수에 저장

            Thread thread = new Thread(new ServerThread());
            thread.start(); //스레드 실행
            Toast.makeText(MainActivity.this, "Connect", Toast.LENGTH_SHORT).show(); //버튼 눌림 이벤트가 발생한걸 알기 위해 그냥 토스트 띄

        }
    };

    public class ServerThread implements Runnable {
        @Override
        public void run() {
            try {
                socket = new Socket(ip, Integer.parseInt(port)); //소켓 연결
                Log.d("connect?", "yes!!!!!!!!!");

                bufferedWriter1 = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), "EUC-KR"));
                sockPrintWriter = new PrintWriter(bufferedWriter1, true);
                String sendMsg = "Phone\n";
                sockPrintWriter.println(sendMsg);  //최초 접속 시 서버에게 "Phone"이라고 보냄

                while(true) {
                    bufferedReader1 = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    while ((line = bufferedReader1.readLine()) != null) {
                        Log.d("content", line); //서버에게서 받은 메시지 로그 찍고
                        func(line); //알림음 울림 기능 수행(아래의 함수로 구현)

                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MainActivity.this, "Server's Msg : " + line, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                    break;
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private void func(String text) {
        Log.d("text", text);
        int text1 = Integer.parseInt(text); //port값은 int형이기 때문에 String을 int로 변환

        if(text1 == 5) {
            mPlayer.start(); //음악재생 루프 false기 때문에 알림음은 약 5초간 재생 후 자동 정지
        }
    }

    View.OnClickListener buttonListener = new View.OnClickListener() {  //레이아웃에서 음소거 버튼을 누르면 자동 정지 이전에도 알림음 정지할 수 있도록
        @Override
        public void onClick(View v) {
            mPlayer.stop();
            try {
                mPlayer.prepare(); //멈추고 다시 동작할 수 있는 준비상태로
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };

    public void onBackPressed() { //뒤로가기 버튼 두번 누르면 앱 종료
        backPressCloseHandler.onBackPressed(); //처음 뒤로가기 누르면 토스트 메시지 띄움. 2초 안에 한번 더 클릭 이벤트가 발생되면 앱 종료
    }

}
