package com.example.pirl.yurim2;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

public class MainActivity extends AppCompatActivity {

    private Button button2, button3;
    private ImageButton imageButton3, imageButton4, imageButton5;
    private ImageButton SpeakerButton;
    private BackPressCloseHandler backPressCloseHandler;
    private AudioManager mAudioManager;
    String[] data;
    private Socket socket;
    BufferedReader in;
    MediaPlayer mPlayer;

    private BufferedReader bufferedReader1;
    private BufferedWriter bufferedWriter1;
    private PrintWriter sockPrintWriter;
    private Handler mHandler;
    private static String line;


    private String ip = "141.223.107.208";
    //private String ip = "141.223.107.219";
    private int port = 9008;
    //private int port = 8004;

    final String filepath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/PhoneNumber/phoneNumber.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toast.makeText(MainActivity.this, "ip : " + ip, Toast.LENGTH_SHORT).show();
        mHandler = new Handler();

        Thread thread = new Thread(new ServerThread());
        thread.start();

        imageButton3 = (ImageButton)findViewById(R.id.imageButton3);
        imageButton3.setOnClickListener(buttonListener1);

        //SpeakerButton = (ImageButton)findViewById(R.id.SpeakerButton);
        //SpeakerButton.setOnClickListener(buttonListener2);

        imageButton4 = (ImageButton)findViewById(R.id.imageButton4);
        imageButton4.setOnClickListener(buttonListener3);

        imageButton5 = (ImageButton)findViewById(R.id.imageButton5);
        imageButton5.setOnClickListener(buttonListener4);

        mAudioManager = (AudioManager)getSystemService(AUDIO_SERVICE); //객체생성
        mAudioManager.setStreamVolume(mAudioManager.STREAM_MUSIC, 100, 0); //max volume

        mPlayer = MediaPlayer.create(MainActivity.this, R.raw.sound2); //노래 파일 생성

        //button2 = (Button)findViewById(R.id.button2);
        //button2.setOnClickListener(buttonListener3);

        backPressCloseHandler = new BackPressCloseHandler(this);

        String read = ReadTextFile(filepath);
        data = read.split("\n");

    }

    public class ServerThread implements Runnable {
        @Override
        public void run() {
            try {
                socket = new Socket(ip, port);
                Log.d("connect?", "yes!!!!!!!!!");

                bufferedWriter1 = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), "EUC-KR"));
                sockPrintWriter = new PrintWriter(bufferedWriter1, true);
                String sendMsg = "Phone\n";
                sockPrintWriter.println(sendMsg);

                while(true) {
                    bufferedReader1 = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    while ((line = bufferedReader1.readLine()) != null) {
                        Log.d("content", line);
                        func(line);

                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(MainActivity.this, "line : " + line, Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                    break;
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private void func(String text) {
        Log.d("text", text);
        int text1 = Integer.parseInt(text);
        int num;

        if(text1 == 4 || text1 == 6){
            if(text1 == 4) num = 0;
            else num = 1;
            //os 버전이 마시멜로우 버전인지 확인
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                //단말기가 전화걸기 권한 허용되어 있는지 확인
                int permissionResult = checkSelfPermission(Manifest.permission.CALL_PHONE);

                //권한 없을 때
                //사용자가 한번이라도 권한을 거부한 적 있는지 확인, 거부한 이력 있으면 true, 없으면 false 반환
                if (permissionResult == PackageManager.PERMISSION_DENIED) {
                    if (shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE)) {
                        AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
                        dialog.setTitle("권한이 필요합니다.")
                                .setMessage("전화걸기 권한 필요합니다. 계속?")
                                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //마시멜로우 버전인지 확인
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                            requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, 1000);
                                        }
                                    }
                                })
                                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Toast.makeText(MainActivity.this, "기능을 취소했습니다.", Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .create()
                                .show();
                    } else { //최초로 권한 요청할 때
                        //CALL_PHONE 권한 안드로이드에 요청
                        requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, 1000);
                    }
                } else { //권한 있을 때
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + data[num]));
                    startActivity(intent);
                }
            }
            else { //os가 마시멜로우 버전 이하일 때
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + data[num]));
                startActivity(intent);
            }
        }

        if(text1 == 5) {
            mPlayer.start();
            //mPlayer.setLooping(true);
        }
    }

    public String ReadTextFile(String path) {
        StringBuffer strBuffer = new StringBuffer();
        try {
            InputStream is = new FileInputStream(path);
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line  = "";
            while((line = reader.readLine()) != null) {
                strBuffer.append(line + "\n");
            }
            reader.close();
            is.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return strBuffer.toString();

    }

    View.OnClickListener buttonListener3 = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //threadstart();
            Thread thread = new Thread(new ServerThread());
            thread.start();
        }
    };

    View.OnClickListener buttonListener4 = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), DevelopeActivity.class);
            startActivity(intent);
            finish();
        }
    };

    /*View.OnClickListener buttonListener2 = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            mPlayer.stop();
            try {
                mPlayer.prepare();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    };*/

    View.OnClickListener buttonListener1 = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), NextActivity.class);
            startActivity(intent);
            finish();
        }
    };

    public void onBackPressed() {
        backPressCloseHandler.onBackPressed();
    }

}
