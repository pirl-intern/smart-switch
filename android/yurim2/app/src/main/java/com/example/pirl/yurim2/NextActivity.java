package com.example.pirl.yurim2;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.ContactsContract;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class NextActivity extends AppCompatActivity {

    private Button button;
    private EditText edittext1, edittext2;
    private ImageButton imageButton, imageButton2, imageButton3, imageButton4;
    final String foldername = Environment.getExternalStorageDirectory().getAbsolutePath() + "/PhoneNumber/";
    final String filename = "phoneNumber.txt";
    static int separate = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.next_activity);

        edittext1 = (EditText)findViewById(R.id.editText1);
        edittext2 = (EditText)findViewById(R.id.editText2);
        //button = (Button)findViewById(R.id.button);
        //button.setOnClickListener(buttonListener);
        imageButton = (ImageButton)findViewById(R.id.imageButton);
        imageButton2 = (ImageButton)findViewById(R.id.imageButton2);
        imageButton3 = (ImageButton)findViewById(R.id.imageButton3);
        imageButton4 = (ImageButton)findViewById(R.id.imageButton4);
        imageButton.setOnClickListener(imageButtonListener);
        imageButton2.setOnClickListener(imageButtonListener1);
        imageButton3.setOnClickListener(imageButtonListener2);
        imageButton4.setOnClickListener(imageButtonListener3);

    }

    View.OnClickListener imageButtonListener2 = new View.OnClickListener() {
        @Override
        public void onClick (View v){
            String contents = edittext1.getText().toString() + "\n" + edittext2.getText().toString();
            WriteTextFile(foldername, filename, contents);
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        }
    };

    View.OnClickListener imageButtonListener3 = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        }
    };

    public void onBackPressed() {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
    }

    public void permissionCheck() {
        //os 버전이 마시멜로우 버전인지 확인
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //단말기에 권한 허용되어 있는지 확인
            int permissionResult = checkSelfPermission(Manifest.permission.READ_CONTACTS);

            //권한 없을 때
            //사용자가 한번이라도 권한을 거부한 적 있는지 확인, 거부한 이력 있으면 true, 없으면 false 반환
            if (permissionResult == PackageManager.PERMISSION_DENIED) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.READ_CONTACTS)) {
                    AlertDialog.Builder dialog = new AlertDialog.Builder(NextActivity.this);
                    dialog.setTitle("권한이 필요합니다.")
                            .setMessage("접근 권한 필요합니다. 계속?")
                            .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //마시멜로우 버전인지 확인
                                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                        requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, 1000);
                                    }
                                }
                            })
                            .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Toast.makeText(NextActivity.this, "기능을 취소했습니다.", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .create()
                            .show();
                } else { //최초로 권한 요청할 때
                    requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, 1000);
                }
            }
        }

    }

    View.OnClickListener imageButtonListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Toast.makeText(NextActivity.this, "click!", Toast.LENGTH_SHORT).show();

            permissionCheck();
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setData(ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
            separate = 1;
            startActivityForResult(intent, 0);

        }
    };

    View.OnClickListener imageButtonListener1 = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Toast.makeText(NextActivity.this, "click!", Toast.LENGTH_SHORT).show();

            permissionCheck();
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setData(ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
            separate = 2;
            startActivityForResult(intent, 0);

        }
    };

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK)
        {
            Cursor cursor = getContentResolver().query(data.getData(),
                    new String[]{ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                            ContactsContract.CommonDataKinds.Phone.NUMBER}, null, null, null);
            cursor.moveToFirst();
            String number = cursor.getString(1);   //1은 번호를 받아옴
            cursor.close();

            if(separate == 1) edittext1.setText(number);
            else edittext2.setText(number);
        }
    }

    public void WriteTextFile(String foldername, String filename, String contents) {
        File dir = new File(foldername);
        if(!dir.exists()) {
            dir.mkdir();
        }
        try {
            //File savefile = new File(foldername + filename);
            BufferedWriter bfw = new BufferedWriter(new FileWriter(foldername + filename, false));
            bfw.write(contents);
            bfw.flush();
            bfw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
