#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 16 19:17:36 2018

@author: pirl
"""

import socket
import sys
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setup(17, GPIO.OUT)
GPIO.output(17,True)

def off():
    GPIO.output(17,True)
    print("off")
    
def on():
    GPIO.output(17,False)
    print("on")

def phone():
    #GPIO.output(17,False)
    print("phone")

def tv():
    #GPIO.output(17,False)
    print("tv")
def Non():
    #GPIO.output(17,False)
    print("None")

commands = {"0": on,
           "1": off,
            "2": phone,
            "3": tv,
            "4": Non,
           }

HOST = '0.0.0.0' # 서버 IP 주소 입력
PORT = int(sys.argv[1])

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
    sock.connect((HOST, PORT))
    sock.send("switch1".encode()) # switch1 이름 전달
    print("waiting order")
    try:
        while True:
            data = sock.recv(256).decode()
            if data:
                '''
                if len(data) >= 2:
                    commands[str(data[0])]()
                else:
                    commands[str(data)]()
                '''
                print(data)
            if data =='close':
                raise Exception                
    except:
        sock.send("switch1 closed".encode())
        sock.close()
