#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug  6 16:50:02 2018

@author: pirl
"""

import io
import socket
import struct
import time
import sys
import os
import picamera


HOST = '0.0.0.0' # 서버 IP 주소 입력
PORT=int(sys.argv[1])
end_time = 3000

client_socket = socket.socket()
client_socket.connect((HOST, PORT))
print("connected with server")

def outputs(connection):
    stream = io.BytesIO()
    start = time.time()
    while True:
        # This returns the stream for the camera to capture to
        yield stream
        # Once the capture is complete, the loop continues here
        # (read up on generator functions in Python to understand
        # the yield statement). Here you could do some processing
        # on the image...
        #stream.seek(0)
        #img = Image.open(stream)
        connection.write(struct.pack('<L', stream.tell()))
        connection.flush()
        # Rewind the stream and send the image data over the wire
        stream.seek(0)
        connection.write(stream.read())
        if time.time() - start > end_time:
            break
        
        # Finally, reset the stream for the next capture
        stream.seek(0)
        stream.truncate()

# Make a file-like object out of the connection
connection = client_socket.makefile('wb')

try:
    with picamera.PiCamera() as camera:
        camera.resolution = (640, 480)
        camera.framerate = 30
        # Start a preview and let the camera warm up for 2 seconds
        #camera.start_preview(fullscreen=False, window=(700,300,640,480))
        time.sleep(2)

        # Note the start time and construct a stream to hold image data
        # temporarily (we could write it directly to connection but in this
        # case we want to find out the size of each capture first to keep
        # our protocol simple)
        stream = io.BytesIO()
        camera.capture_sequence(outputs(connection), 'jpeg',use_video_port=True)
            # Write the length of the capture to the stream and flush to
            # ensure it actually gets sent
    # Write a length of zero to the stream to signal we're done
    connection.write(struct.pack('<L', 0))
finally:
    connection.close()
    client_socket.close()
